/****************************************************************************************
 ** Program Filename: Venue.cpp
 ** Author: Valerie Chapple
 ** Date: March 11, 2016
 **
 ** Description:    Operates the Venue class
 ** Input:          User can complete activities, as well as go to a concert
 ** Output:         User can collect an item; complete the task of going to a concert
 ***************************************************************************************/
#include "Venue.hpp"
#include "validation.hpp"

#include <iostream>
#include <string>


/****************************************************************************************
 ** Function:         Venue()
 ** Description:      Default Constructor
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Venue instantiated with initialized values.
 ***************************************************************************************/
Venue::Venue(){
    this->prompt = "";
    this->activity1 = "";
    this->activity2 = "";
    this->activity3 = "";
    this->type = 3;
}



/****************************************************************************************
 ** Function:         ~Venue()
 ** Description:      Deconstructor
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Venue destroyed
 ***************************************************************************************/
Venue::~Venue() {
}


/****************************************************************************************
 ** Function:         converse()
 ** Description:      Converse with Venue only if item Exists
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Choose activity, possibly collecting an item
 ***************************************************************************************/
void Venue::converse(){
    int choice = -1 ;
    
    std::string promptStr = prompt;
    //"\nThere are some fun things to do here!\n";
    promptStr += "\n";
    promptStr += "\t1 - ";
    promptStr += activity1;
    promptStr += "\n";
    promptStr += "\t2 - ";
    promptStr += activity2;
    promptStr += "\n";
    promptStr += "\t3 - ";
    promptStr += activity3;
    promptStr += "\n";
    promptStr += "\t0 - Back\n";
    promptStr += "What would you like to do? ";
    
    choice = validate_non_neg_integer(promptStr, 3);
    std::cout << std::endl;
    std::cout << std::endl;
    switch (choice) {
        case 1:
            /* Do Option 1 and get hidden item */
            std::cout << "Awesome!\n";
            std::cout << std::endl;
            if ((hiddenItem != NULL) && (!itemGone)) {
                std::cout << "Look there's a prize!\n";
                
                /* Prompt to accept item */
                std::cout << "\n" << hiddenItem->getName() << ":\n";
                std::cout << "\"" << hiddenItem->getDescription() << "\"\n\n";
                promptStr = "Do you want to add the item? [0 - no, 1 - yes] ";
                int addItemAnwswer = -1;
                addItemAnwswer = validate_non_neg_integer(promptStr, 1);
                if (addItemAnwswer == 1) {
                    itemGone = true;
                }
            }
            else {
                std::cout << "Hope you had fun!\n\n";
                press_enter();
            }
            clear();
            special();
            std::cout << std::endl;
            break;
            
        case 2:
            /* Do Option 2 */
            std::cout << activity2 << " is a nice Choice!\n\n";
            press_enter();
            clear();
            special();
            std::cout << std::endl;
            break;
        case 3:
            /* Do Option 3 */
            std::cout << activity3 << " isn't my favorite, but have fun!\n\n";
            press_enter();
            clear();
            special();
            std::cout << std::endl;
            break;
        default:
            break;
        
    }
    
    visitStatus = 1;
}



/****************************************************************************************
 ** Function:         special()
 ** Description:      special task to participate at a venue
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  changes specialTask to true if selected. Unchanged otherwise
 ***************************************************************************************/
void Venue::special(){
    /* Prompt user to */
    std::string prompt = "Do you want to stay and watch a concert?\n";
    prompt += "\t 0 - No thanks\n";
    prompt += "\t 1 - Yeah.\n";
    prompt += "\t 2 - My date does, so sure!\n";
    
    int choice = validate_non_neg_integer(prompt, 2);
    
    if (choice != 0){
        specialTaskDone = true;
        std::cout << "Congrats! You had a great time at the concert!\n";
        press_enter();
    }
}



/****************************************************************************************
 ** Function:         setActivities()
 ** Description:      sets prompt from first parameter, all others are activity strings
 ** Parameters:       4 string parameters for venue
 ** Pre-Conditions:   None
 ** Post-Conditions:  Assigns string values to prompt and the three activities.
 ***************************************************************************************/
void Venue::setActivities(std::string p, std::string a1, std::string a2, std::string a3){
    this->prompt = p;
    this->activity1 = a1;
    this->activity2 = a2;
    this->activity3 = a3;
}



/****************************************************************************************
 ** Function:         getPrompt()
 ** Description:      gets prompt
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns string value for prompt
 ***************************************************************************************/
std::string Venue::getPrompt(){
    return prompt;
}



/****************************************************************************************
 ** Function:         getActivity1(), getActivity2(), getActivity3(),
 ** Description:      gets Activity1, Activity2, Activity3
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns string value for Activity1, Activity2, Activity3
 ***************************************************************************************/
std::string Venue::getActivity1(){
    return activity1;
}
std::string Venue::getActivity2(){
    return activity1;
}
std::string Venue::getActivity3(){
    return activity1;
}
