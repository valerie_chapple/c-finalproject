/****************************************************************************************
 ** Program Filename: validation.cpp
 ** Author: Valerie Chapple
 ** Date: January 17, 2016 (updated Mar 9, 2016)
 **
 ** Description:    main functions implementation file
 ** Input:          User data
 ** Output:         Validation feedback on data entry.
 ***************************************************************************************/

#include <iostream>
#include <string>
#include <limits>                   /* Validating User Input */
#include <cstdlib>                  /* Clear Screen */
#include <string>
#include <cctype>
#include <sstream>                  /* intToString() function*/


#include "validation.hpp"
/****************************************************************************************
 ** Function:           clear()
 ** Description:        clear the screen
 ** Parameters:         none
 ** Pre-Conditions:     cstdlib header
 ** Post-Conditions:    clears the console
 ***************************************************************************************/
void clear(){
    std::system("clear");
}



/****************************************************************************************
 ** Function:           pause()
 ** Description:        Hold the current screen until the user presses enter
 ** Parameters:         none
 ** Pre-Conditions:     none
 ** Post-Conditions:    user needs to press enter to continue
 ***************************************************************************************/
void press_enter(){
    std::cout << "Press Enter to Continue.";
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


/****************************************************************************************
 ** Function:         validate_positive_integer
 ** Description:      Allows for a prompt to repeat until a correct menu item is selected
 ** Parameters:       Prompt string and number of integers in menu.
 ** Pre-Conditions:   Integer must be positive non-zero number, or -1. -1 will result in
 **                   no upper limit of positive integers.
 ** Post-Conditions:  menu item selected is returned.
 ***************************************************************************************/
int validate_positive_integer(std::string prompt, int max_integer){
    bool show_prompt = true;
    int choice = 0;
    
    do {
        /* Display Prompt */
        std::cout << prompt;
        
        /* Repeat while input is not an Integer */
        while (!(std::cin >> choice)) {
            std::cout << "Invalid Entry!\n";
            std::cout << prompt;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        
        if (max_integer != -1) {
            /* Limited to the number of menu choices */
            if ((choice <= max_integer) && (choice > 0)){
                show_prompt = false;
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

            }
            else {
                std::cout << "Invalid Entry!\n";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
        }
        
        else {
            /* Choice must be a positive integer */
            if (choice > 0){
                show_prompt = false;
                /* added */
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            else {
                std::cout << "Invalid Entry!\n";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
        }
    }
    while (show_prompt);
    
    return choice;
}




/****************************************************************************************
 ** Function:          validate_integer()
 ** Description:       Validates that an integer has been entered.
 ** Parameters:        boolean to continue entering data (handled by function caller)
 ** Pre-Conditions:    Prompt already shown, loop set up to handle reference parameter
 ** Post-Conditions:   returns the integer, updates runLoop if 'q' or 'Q' typed.
 ***************************************************************************************/

int validate_integer(bool &runLoop){
    bool getValue = true;
    int number;
    while (getValue) {
        std::string input = "";
        const char * cStr;
        int length;
        std::getline(std::cin, input);
        length = input.length();
        cStr = input.c_str();
        
        number = atof(cStr);
        
        if ( (input == "q") || (input == "Q")) {
            runLoop = false;
            getValue = false;
            number = 0;
        }
        else if (input ==""){
            getValue = true;
        }
        else {
            getValue = false;
            for (int i = 0; i < length; i++){
                if ((cStr[0] == '-') && (length != 1)){
                }
                else if (isdigit(cStr[i])) {
                }
                else {
                    //cStr = NULL;
                    std::cout << "******************************************\n";
                    std::cout << "Invalid data! Last entry not included!\n";
                    std::cout << "******************************************\n";
                    std::cin.clear();
                    getValue = true;
                    length = 0;
                }
            }
        }
    }
    
    /* Display number if 'q' not entered */
    if (runLoop) {
        std::cout << "\tAccepted: " << number << std::endl;
    }
    return number;
}







/****************************************************************************************
 ** Function:         validate_non_neg_integer
 ** Description:      Allows for a prompt to repeat until a correct menu item is selected
 ** Parameters:       Prompt string and number of integers in menu.
 ** Pre-Conditions:   Integer must be positive non-zero number, or -1. -1 will result in
 **                   no upper limit of positive integers.
 ** Post-Conditions:  menu item selected is returned.
 ***************************************************************************************/

int validate_non_neg_integer(std::string prompt, int max_integer){
    bool show_prompt = true;
    int choice;
    do {
        /* Display Prompt */
        std::cout << prompt;
        
        /* Repeat while input is not an Integer */
        while (!(std::cin >> choice)) {
            std::cout << "Invalid Entry!\n";
            std::cout << prompt;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        
        if (choice < 0) {
            std::cout << "Invalid Entry!\n";
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else {
            if (max_integer != -1) {
                /* Limited to the number of menu choices */
                if ((choice <= max_integer) && (choice >= 0)){
                    show_prompt = false;
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
                else {
                    std::cout << "Invalid Entry!\n";
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
            }
            
            else {
                /* Choice must be a non negative integer */
                if (choice >= 0){
                    show_prompt = false;
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
                else {
                    std::cout << "Invalid Entry!\n";
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
            }
        }
    }
    while (show_prompt);
    
    return choice;
}






/****************************************************************************************
 ** Function:         validate_non_neg_double(std::string prompt)
 ** Description:      Allows for a prompt to repeat until a positiv double is entered.
 ** Parameters:       Prompt string
 ** Pre-Conditions:   Prompt correctly entered
 ** Post-Conditions:  Returns double with the user's positive double response.
 ***************************************************************************************/

double validate_non_neg_double(std::string prompt){
    bool show_prompt = true;
    double choice;
    do {
        /* Display Prompt */
        std::cout << prompt;
        
        /* Repeat while input is not an Integer */
        while (!(std::cin >> choice)) {
            std::cout << "Invalid Entry!\n";
            std::cout << prompt;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        
        if (choice < 0) {
            std::cout << "Invalid Entry!\n";
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else {
            show_prompt = false;
        }
    }
    while (show_prompt);
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return choice;
}





/****************************************************************************************
 ** Function:          validate_double()
 ** Description:       Validates that a double has been entered.
 ** Parameters:        string for prompt, integer of max menu choices (default: -1)
 ** Pre-Conditions:    Prompt contains menu option or question. Max_integer of -1 extends
 **                     all possible input to all positive integers.
 ** Post-Conditions:   Prints string in reverse followed by a newline character
 ***************************************************************************************/

double validate_double(bool &runLoop){
    std::string input = "";
    const char * cStr;
    int length;
    
    bool getDouble = true;
    double number = 0;
    
    while (getDouble) {
        std::getline(std::cin, input);
        length = input.length();
        cStr = input.c_str();
        
        number = atof(cStr);
        
        if ( (input == "q") || (input == "Q")) {
            runLoop = false;
            getDouble = false;
            number = 0;
        }
        else {
            for (int i = 0; i < length; i++){
                if (cStr[i] == '.') {
                }
                else if (cStr[i] == '-') {
                }
                else if (( cStr[i] > 47 ) && ( cStr[i] <= 57 )){
                }
                else {
                    cStr = NULL;
                    length = 0;
                }
            }
            
            if (cStr == NULL) {
                std::cout << "******************************************\n";
                std::cout << "Invalid data! Last entry not included!\n";
                std::cout << "******************************************\n";
                std::cin.clear();
                getDouble = true;
            }
            else {
                if ( (number == 0) && (cStr[0] != '0' ) ) {
                    /* Number is zero, but the string was not */
                    std::cout << "******************************************\n";
                    std::cout << "Invalid data! Last entry not included!\n";
                    std::cout << "******************************************\n";
                    std::cin.clear();
                    getDouble = true;
                }
                else {
                    getDouble = false;
                }
            }
        }
    }
    
    return number;
}




/****************************************************************************************
 ** Function:         getNonBlankAlphaNumUserString
 ** Description:      Allows for a prompt to repeat until a nonempty string is entered.
 ** Parameters:       Prompt string
 ** Pre-Conditions:   Prompt correctly entered, spaces are allowed
 ** Post-Conditions:  Returns string with the user's non blank alphanumberic response
 ***************************************************************************************/
std::string getNonBlankAlphaNumUserString(std::string prompt){
    std::string input = "";
    const char * cstr;
    bool repeatFlag = true;
    std::cout << prompt;
    while (repeatFlag == true) {
        while (!(std::getline(std::cin, input))) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Error: Invalid entry.\n" << prompt;
        }
        if (input == ""){
            repeatFlag = true;
            std::cout << "Error: Input cannot be blank.\n" << prompt;
        }
        int length = input.length();
        cstr = input.c_str();
        
        /* Check that input is only alphanumeric and/or spaces */
        int index = 0;
        
        
        while (isprint(cstr[index])) {
            index++;
        }
        
        
        if (length != index){
            repeatFlag = true;
            std::cout << "Error: Must be alphanumeric.\n" << prompt;
        }
        else if (isspace(cstr[0])){
            repeatFlag = true;
            std::cout << "Error: Cannot begin with a space.\n" << prompt;
        }
        else {
            repeatFlag = false;
        }
    }
    return input;
}




/****************************************************************************************
 ** Function:         getNonBlankAlphaNumUserString
 ** Description:      Allows for a prompt to repeat until a nonempty string is entered.
 ** Parameters:       Prompt string
 ** Pre-Conditions:   Prompt correctly entered
 ** Post-Conditions:  Returns string with the user's non blank alphanumberic response
 ***************************************************************************************/
std::string getNonBlankAlphaNumUserStringNoSpace(std::string prompt){
    std::string input = "";
    const char * cstr;
    bool repeatFlag = true;
    std::cout << prompt;
    while (repeatFlag == true) {
        while (!(std::getline(std::cin, input))) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Error: Invalid entry.\n" << prompt;
        }
        
        if ((input == "") || (input == "\n")) {
            repeatFlag = true;
            std::cout << "Error: Input cannot be blank.\n" << prompt;
        }
        else {
            int length = input.length();
            cstr = input.c_str();
            
            /* Check that input is only alphanumeric, no spaces*/
            int index = 0;
            while (isalnum(cstr[index])) {
                index++;
            }
            
            if (length != index){
                repeatFlag = true;
                std::cout << "Error: Must be alphanumeric.\n" << prompt;
            }
            else if (isspace(cstr[0])){
                repeatFlag = true;
                std::cout << "Error: Cannot begin with a space.\n" << prompt;
            }
            else {
                repeatFlag = false;
            }
        }
    }
    return input;
}



/****************************************************************************************
 ** Function:         intToString(int)
 ** Description:      Allows for a integer to be converted to string
 ** Parameters:       input
 ** Pre-Conditions:   any integer
 ** Post-Conditions:  Returns string with no space around integer
 ***************************************************************************************/
std::string intToString(int input){
    std::stringstream ss;
    
    /* Update Attack Message */
    ss << input;
    std::string message = ss.str();
    
    return message;
}
