/****************************************************************************************
 ** Program Filename: Location.hpp
 ** Author: Valerie Chapple
 ** Date: March 9, 2016
 **
 ** Description:    Implementation of the Location Class
 ** Input:          User chooses next location based on pointers of Location
 ** Output:         Shows current location, as well as choices for future locations
 ***************************************************************************************/

#ifndef LOCATION_HPP
#define LOCATION_HPP
#include "Item.hpp"

#include <string>

class Location {
protected:
    std::string name;
    std::string description;
    Location * p1;
    Location * p2;
    Location * p3;
    Location * p4;
    
    int type;               /* 1 - Restaurant, 2 - Store, 3 -  Venue*/
    bool specialTaskDone;
    
    Item * hiddenItem;
    bool itemGone;
    Item * redeemableItem;
    
    int visitStatus;                                /* 0 - no convo; 1 - repeat convo */
    std::string customTrivial;                      /* String for No choices */
    
public:
    Location();
    virtual ~Location();
    
    /* Public Methods */
    virtual void converse() = 0;
    virtual void special() = 0;
    void interact();
    Location * chooseNextLocation();
    
    
    /* Get/Set Methods */
    void setName(std::string);
    std::string getName();
    void setDescription(std::string);
    std::string getDescription();
    void setPlaces(Location * p1, Location * p2, Location * p3, Location * p4);
    Location * getP1();
    Location * getP2();
    Location * getP3();
    Location * getP4();
    int getType();
    bool isSpecialTaskDone();
    
    /* Get/Set Items Methods */
    void setItem(Item * );
    Item * getItem();
    bool isItemGone();
    void returnItem();
    void removeItem();
    void setRedeemableItem(Item *);
    Item * getRedeemableItem();
    
    /* Get/Set Interacting Methods */
    int getVisitStatus();
    void setVisitStatus(int status);
    std::string getCustomTrivial();
    void setCustomTrivial(std::string);
};

#endif