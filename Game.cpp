/****************************************************************************************
 ** Program Filename: Game.cpp
 ** Author: Valerie Chapple
 ** Date: March 11, 2016
 **
 ** Description:    Game Class to manage octoBeR AlwaYs
 ** Input:          User selects locations to which to travel and explore.
 ** Output:         User can win when certain items are collected and redeemed, as well
 **                 as when three particular actions are taken.
 ***************************************************************************************/

#include "Game.hpp"
#include "Location.hpp"
#include "Venue.hpp"
#include "Shop.hpp"
#include "Restaurant.hpp"

#include "validation.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

/****************************************************************************************
 ** Function:         Game()
 ** Description:      Default Constructor
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Game instantiated with initialized values.
 ***************************************************************************************/

Game::Game() {
    quitGame = false;
    this->moveCount = 0;
    this->currentLocation = NULL;
    tipRestaurant = false;
    talkShop = false;
    playVenue = false;
}




/****************************************************************************************
 ** Function:         ~Game()
 ** Description:      Destructor
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Deletes memory allocated if not NULL
 ***************************************************************************************/

Game::~Game() {
    std::cout << "Destructor Game\n";
    
    /* Delete locations */
    while (places.size() != 0) {
        if (places.back() != NULL) {
            delete places.back();
            places.back() = NULL;
        }
        places.pop_back();
    }
    /* Items deleted in locations, as it is a 1-1 ratio and never changes */
}

/****************************************************************************************
 ** Function:         play()
 ** Description:      Starts and finishes the entire game play
 ** Parameters:       None
 ** Pre-Conditions:   Game is instantiated
 ** Post-Conditions:  Runs the phases of the game: Setup, Play, End
 ***************************************************************************************/

void Game::start() {
    /* Set Up Game */
    clear();
    
    std::cout << "\n*****************************************\n";
    std::cout <<   "**                                     **\n";
    std::cout <<   "**            octoBeR AlwaYs           **\n";
    std::cout <<   "**                                     **\n";
    std::cout <<   "*****************************************\n\n\n";
    
    std::cout <<   "*****************************************\n";
    std::cout <<   "**                                     **\n";
    std::cout <<   "**  October 27, 2015                   **\n";
    std::cout <<   "**                                     **\n";
    std::cout <<   "**  You are flying to Kansas City, MO  **\n";
    std::cout <<   "**  to go on a date with a long lost   **\n";
    std::cout <<   "**  love.                              **\n";
    std::cout <<   "**                                     **\n";
    std::cout <<   "**  You only have a limited number of  **\n";
    std::cout <<   "**  moves to orchestrate the perfect   **\n";
    std::cout <<   "**  date with this Kansas City native. **\n";
    std::cout <<   "**                                     **\n";
    std::cout <<   "**  Collect hidden items from places   **\n";
    std::cout <<   "**  on the map.  Then redeem them at   **\n";
    std::cout <<   "**  other locations. Find and redeem   **\n";
    std::cout <<   "**  all the key items to earn another  **\n";
    std::cout <<   "**  date with the love of your life.   **\n";
    std::cout <<   "**                                     **\n";
    std::cout <<   "**  Be careful, though, as only some   **\n";
    std::cout <<   "**  items are useful for the date, and **\n";
    std::cout <<   "**  you have a maximum number of items **\n";
    std::cout <<   "**  you can keep at a time.            **\n";
    std::cout <<   "**                                     **\n";
    std::cout <<   "**  Hinte: Don't forget to be on your  **\n";
    std::cout <<   "**  best behavior with your date and   **\n";
    std::cout <<   "**  other Kansas Citians you meet.     **\n";
    std::cout <<   "**                                     **\n";
    std::cout <<   "**  Best of Luck!!!                    **\n";
    std::cout <<   "**                                     **\n";
    std::cout <<   "*****************************************\n\n\n";
    
    std::string prompt = "Press 1 to see a cheat sheet: [0 - no] ";
    
    if (validate_non_neg_integer(prompt, 1) == 1) {
        /* Open cheat sheet file and display */
        clear();
        std::string filename = CHEAT_SHEET_FILENAME;
        std::ifstream inputFile;
        inputFile.open(filename.c_str());
        
        
        if (inputFile.fail()) {
            std::cout << "Cannot open the file.\n";
        }
        else {
            std::string inputline;
            while (!inputFile.eof()){
                std::getline(inputFile, inputline);
                std::cout << inputline << std::endl;
            }
        }
        std::cout << std::endl;
        std::cout << std::endl;
        inputFile.close();
        press_enter();
    }
    setup();
    
    /* Play All Locations Until Game is over */
    clear();
    play();
    
    /* End Game */
    end();
}


/****************************************************************************************
 ** Function:         setup()
 ** Description:      Calls a specific city location to implement
 ** Parameters:       None
 ** Pre-Conditions:   Game is instantiated
 ** Post-Conditions:  Sets up Game places using a function for the city specified
 ***************************************************************************************/
void Game::setup() {
    startKansasCity();
    printFile(LOGO_FILENAME);
    press_enter();
}



/****************************************************************************************
 ** Function:         play()
 ** Description:      Plays game until time runs out, user quits, or game is won
 **                   Controls the player's location based on arrive() function return
 ** Parameters:       None
 ** Pre-Conditions:   Game setup has been called
 ** Post-Conditions:  Game is over at end of function call
 ***************************************************************************************/
void Game::play() {
    if ((places.empty()) && (places.size() < 4)){
        std::cout << "Internal Error: Not enough places allocated for game.\n";
    }
    else {
        /* User chooses starting location */
        int choice = 0;
        printFile(MAP_FILENAME);
        
        std::cout << "\n\nYou've landed in Kansas City at the DownTown Airport.\n";
        std::cout << "Where would you like to go?\n";
        std::cout << "\t1 - " << places.at(0)->getName() << "\n";
        std::cout << "\t2 - " << places.at(1)->getName() << "\n";
        std::cout << "\t3 - " << places.at(2)->getName() << "\n";
        std::cout << "\t4 - " << places.at(3)->getName() << "\n";
        choice = validate_positive_integer("\nWhich option? ", 4);
        
        switch (choice) {
            case 1:
                /* Choice 1 selected */
                currentLocation = places.at(0);
                break;
            case 2:
                /* Choice 1 selected */
                currentLocation = places.at(1);
                break;
            case 3:
                /* Choice 1 selected */
                currentLocation = places.at(2);
                break;
            case 4:
                /* Choice 1 selected */
                currentLocation = places.at(3);
                break;
            default:
                break;
        }
        clear();

        while ((!isGameOver()) && (!quitGame)) {
            /* Play game until game is over */
            Location * newLocation = arrive();
            if (newLocation == NULL) {
                /* Game over because items are collected and redeemed or user quit */
            }
            else if (newLocation == currentLocation){
                /* Player is staying stationary, do not change moveCount */
            }
            else {
                /* Move player to new location and change moveCount */
                currentLocation = newLocation;
                moveCount++;
                
                std::cout << "******************************************************\n";
                std::cout << "CURRENT LOCATION: " << currentLocation->getName() << "\n";
                std::cout << "PLAYER STATUS: " << std::endl;
                std::cout << "Moves Used: " << moveCount << " of " << MAX_MOVES << "\n";
                if (tipRestaurant) { std::cout << "Restaurant Task: Check\n"; }
                if (talkShop) { std::cout << "Shop Task: Check\n"; }
                if (playVenue) { std::cout << "Venue Task: Check\n"; }
                printItemsCollected();
                printItemsUsed();
                std::cout << "******************************************************\n";
                press_enter();

            }
        }
    }
}



/****************************************************************************************
 ** Function:         end()
 ** Description:      Runs after setup() and play() functions have been called
 ** Parameters:       None
 ** Pre-Conditions:   Game::setup() is called then Game::play() prior to Game::end()
 ** Post-Conditions:  Game is over, results are determined.
 ***************************************************************************************/
void Game::end() {
    clear();
    if (doKeysMatch()) {
        std::cout << "Congratulations, you won a second date!!!!\n";
        playWinningScene();
    }
    else if (quitGame){
        std::cout << "You quit! Try again.\n";
    }
    else if (moveCount >= (MAX_MOVES)){
        std::cout << "Not quite! You ran out of time! Try again.\n";
    }
    else {
        std::cout << "Try again.\n";
    }
    
    std::cout << "Game Over.\n";
    
    press_enter();
    clear();
}




/****************************************************************************************
 ** Function:         arrive()
 ** Description:      Begins interaction with the location
 **                     - Greet Player
 **                     - Give options for exploring or leaving location or quit game
 **                     - Handle user choice, redeeming collected items when exploring
 **
 ** Parameters:       None
 ** Pre-Conditions:   Called during Game::play()
 ** Post-Conditions:  Controls the action while in a particular location
 ***************************************************************************************/
Location * Game::arrive(){
    Location * newLocation = NULL;
    int choice = 1;
    while (choice == 1) {
        clear();
        std::cout << std::endl;
        /* Greet Player */
        if (currentLocation->getVisitStatus() == 0) {
            /* First time visiting */
            std::cout << "Welcome to ";
        }
        else {
            std::cout << "Welcome back to ";
        }
        std::cout << currentLocation->getName() << ".\n";
        
        /* Redeem Specific Item, possibly */
        if (itemAlreadyCollected(currentLocation->getRedeemableItem())) {
            std::cout << "You have an item to use!!!!!\n";
            Item * item = currentLocation->getRedeemableItem();
            std::cout << "...Redeeming " << item->getName() << std::endl;
            std::cout << std::endl;
            press_enter();
            /* Move Key Item from items to itemsUsed */
            removeItem(item);
            itemsUsed.push_back(item);
            choice = -1;
        }
        else {
            /* Prompt user options */
            std::cout << "Choices: \n";
            std::cout << "\t1 - Explore " << currentLocation->getName() << std::endl;
            std::cout << "\t2 - Go somewhere else" << std::endl;
            std::cout << "\t0 - Quit" << std::endl;
            choice = validate_non_neg_integer("What would you like to do? ", 2);
            
            /* User chooses to Explore location */
            if (choice == 1) {
                /* Interact with Location */
                currentLocation->interact();
                
                /* Determine if Special task was executed */
                if (currentLocation->isSpecialTaskDone()) {
                    int locationType = currentLocation->getType();
                    /* 1 - Restaurant, 2 - Shop, 3 - Venue */
                    switch (locationType) {
                        case 1:
                            /* Restaurant Special Task completed */
                            tipRestaurant = true;
                            break;
                        case 2:
                            /* Shop Special Task completed */
                            talkShop = true;
                            break;
                        case 3:
                            /* Venue Special Task completed */
                            playVenue = true;
                            break;
                        default:
                            break;
                    }
                }
                
                /* Collect items if exists and has not been collected or used. */
                if (currentLocation->getItem() != NULL) {
                    if (currentLocation->isItemGone()) {
                        Item * temp = currentLocation->getItem();
                        if (itemAlreadyCollected(temp) || itemAlreadyUsed(temp)) {
                            /* Do not add item */
                        }
                        else {
                            /* Check that item vector size is not above the limit */
                            bool addItem = true;
                            while ((items.size() >= MAX_ITEMS_CARRIED) && (addItem)) {
                                /* Limit Reached */
                                clear();
                                std::cout << "Can't add the item because";
                                std::cout << " you have too many items!!!!\n";
                                addItem = removeItemsMenu();
                            }
                            if (addItem) {
                                /* Add Item: it has not been collected or used */
                                items.push_back(currentLocation->getItem());
                            }
                            else {
                                std::cout << "Item was not added.\n";
                                currentLocation->returnItem();
                            }
                            printItemsCollected();
                            press_enter();
                            
                        }
                    }
                }
                
                /* Keys are collected and tasks done prior */
                if (isGameOver()){
                    choice = -1;
                }
                
                
            }
        }
    }

    /* Determine return location */
    if (choice == 0) {
        /* If choice is to Quit */
        quitGame = true;
        newLocation = NULL;
    }
    else if(choice == -1){
        /* Game over because tasks and keys determiend, so stay at current location */
        newLocation = currentLocation;
    }
    else {
        if (!doKeysMatch()) {
            /* Show other place options */
            clear();
            printFile(MAP_FILENAME);
            newLocation = currentLocation->chooseNextLocation();
            clear();
        }
    }

    return newLocation;
}




/****************************************************************************************
 ** Function:         isGameOver()
 ** Description:      Checks that the time hasn't run out and items are in the itemsUsed
 ** Parameters:       None
 ** Pre-Conditions:   Game is instantiated
 ** Post-Conditions:  Returns true if game is over. returns false if game is not over.
 ***************************************************************************************/
bool Game::isGameOver(){
    bool result = true;
    /* For each item in keyItem */
    
    if (moveCount >= (MAX_MOVES)) {
        /* Ran out of time!!! */
    }
    else {
        if (doKeysMatch()){
            /* Keys match */
        }
        else {
            result = false;
        }
    }
    
    return result;
}


/****************************************************************************************
 ** Function:         doKeysMatch()
 ** Description:      Checks that the three items are in itemsUsed vector Place Venues
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns true if all each venue special function has been called and
 **                   keyItems vector are in the itemsUsed vector
 ***************************************************************************************/
bool Game::doKeysMatch(){
    bool result = true;
    
    /* Check that a Venue, Shop, and Restaurant Interaction has occurred */
    if (!tipRestaurant) {
        result = false;
    }
    else if (!talkShop) {
        result = false;
    }
    else if (!playVenue) {
        result = false;
    }

    /* If result is still true, then check if keyItems have been used yet */
    if (result) {
        for (unsigned int i = 0; i < keyItems.size(); i++) {
            int matchFound = false;
            /* Find match in itemsUsed */
            for (unsigned int j = 0; j < itemsUsed.size(); j++) {
                if (keyItems.at(i) == itemsUsed.at(j)) {
                    matchFound = true;
                    j = itemsUsed.size();   /* Stop this item's search */
                }
            }
            if (!matchFound) {
                /* Match not found, end loop and return game is not over */
                result = false;
            }
        }
    }
    
    return result;
}



/****************************************************************************************
 ** Function:         itemAlreadyCollected()
 ** Description:      Determines if Item has already been collected
 ** Parameters:       Pointer to item
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns true if Item has already been collected
 ***************************************************************************************/
bool Game::itemAlreadyCollected(Item * test){
    bool result = false;
    for (unsigned int i = 0; i < items.size(); i++) {
        if (items.at(i) == test) {
            /* Item is in items vector */
            result = true;
        }
    }
    return result;
}



/****************************************************************************************
 ** Function:         itemAlreadyUsed()
 ** Description:      Determines if Item has already been used
 ** Parameters:       Pointer to item
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns true if Item has already been used
 ***************************************************************************************/
bool Game::itemAlreadyUsed(Item * test){
    bool result = false;
    for (unsigned int i = 0; i < itemsUsed.size(); i++) {
        if (itemsUsed.at(i) == test) {
            /* Item is in usedItems vector */
            result = true;
        }
    }
    return result;
}


/****************************************************************************************
 ** Function:         removeItem()
 ** Description:      Removes Item from vector
 ** Parameters:       Item *
 ** Pre-Conditions:   Does not check for multiple items, not if item is not present
 ** Post-Conditions:  Removes Item from items vector
 ***************************************************************************************/
void Game::removeItem(Item * item){
    /* Find and remove item parameter in items Carried */
    for (unsigned int i = 0; i < items.size(); i++) {
        if (items.at(i) == item) {
            /* Erase Item */
            items.erase(items.begin() + i);
        }
    }
}


/****************************************************************************************
 ** Function:         removeItemsMenu()
 ** Description:      Offers menu to remove item or not add item
 ** Parameters:       Item *
 ** Pre-Conditions:   Let
 ** Post-Conditions:  Removes Item from items vector
 ***************************************************************************************/
bool Game::removeItemsMenu(){
    bool addItem = true;
    std::string prompt = "Select an item to remove:\n";
    int size = items.size();
    for (int i = 0; i < size; i++) {
        prompt += "\t";
        prompt += intToString(i + 1);
        prompt += " - ";
        prompt += items.at(i)->getName();
        prompt += "\n";
    }
    prompt += "\n\t0 - Keep all current items.\n";

    int choice = validate_non_neg_integer(prompt, size);
    
    switch (choice) {
        case 0:
            addItem = false;
            break;
        default:
            choice--;
            /* Return item to StartingLocation */
            items.at(choice)->getStartLocation()->returnItem();
            /* Remove from carried items vector */
            items.erase(items.begin() + choice);
            break;
    }
    return addItem;
}

/****************************************************************************************
 ** Function:         printItemsCollected()
 ** Description:      Print item name and location found to console of items collected
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Prints to console any items collected
 ***************************************************************************************/
void Game::printItemsCollected(){
    /* Print name and Location of Vector of Items */
    if (!items.empty()) {
        std::cout << "Current Items (max of " <<  MAX_ITEMS_CARRIED << "):\n";
        for (unsigned int i = 0; i < items.size(); i++){
            Item * hiddenItem = items.at(i);
            std::cout << "\t" << hiddenItem->getName();
            if (hiddenItem->getStartLocation() !=NULL) {
                std::cout << " found at ";
                std::cout << hiddenItem->getStartLocation()->getName() << std::endl;
            }
        }
        std::cout << std::endl;
    }

}



/****************************************************************************************
 ** Function:         printItemsUsed()
 ** Description:      Print item name and location found to console of items used
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Prints to console any items used
 ***************************************************************************************/
void Game::printItemsUsed(){
    /* Print name and Location of Vector of Items Used */
    if (!itemsUsed.empty()) {
        std::cout << "Items You Have Used:\n";
        for (unsigned int i = 0; i < itemsUsed.size(); i++){
            Item * hiddenItem = itemsUsed.at(i);
            std::cout << "\t" << hiddenItem->getName();
            if (hiddenItem->getStartLocation() !=NULL) {
                std::cout << " found at ";
                std::cout << hiddenItem->getStartLocation()->getName() << std::endl;
            }
        }
        std::cout << std::endl;
    }
}



/****************************************************************************************
 ** Function:         printFile()
 ** Description:      file as given
 ** Parameters:       None
 ** Pre-Conditions:   filename must be defined
 ** Post-Conditions:  Prints file to console
 ***************************************************************************************/

void Game::printFile(std::string filename){
        clear();
        std::ifstream inputFile;
        inputFile.open(filename.c_str());
        
        if (inputFile.fail()) {
            std::cout << "Cannot open the file.\n";
        }
        else {
            std::string inputline;
            while (!inputFile.eof()){
                std::getline(inputFile, inputline);
                std::cout << inputline << std::endl;
            }
        }
        inputFile.close();
}



/****************************************************************************************
 ** Function:         playWinningScene()
 ** Description:      Play "animation" for winning scene
 ** Parameters:       None
 ** Pre-Conditions:   Must have checked that player won.
 ** Post-Conditions:  Shows images with usleep() to simulate an animation
 ***************************************************************************************/

void Game::playWinningScene(){
    int speed = 300000;
    int speed2 = 900000;
    usleep(speed2);
    for (int i = 0; i < 5; i++){
        printFile(WIN_FILENAME0);
        usleep(speed);
        printFile(WIN_FILENAME1);
        usleep(speed);
        printFile(WIN_FILENAME2);
        usleep(speed2);
    }
}


/****************************************************************************************
 ** Function:         startKansasCity()
 ** Description:      initialize map of places for kansas city
 ** Parameters:       None
 ** Pre-Conditions:   SetUp Kansas City Map of places
 ** Post-Conditions:  location vector has Kansas city locations
 ***************************************************************************************/

void Game::startKansasCity(){
    
    /* 
     * Venues
     */
    std::string prompt = "";
    std::string str1 = "";
    std::string str2 = "";
    std::string str3 = "";
    
    /* UpDown Arcade, play Mrs. PacMan to get a coupon to Waldo Pizza */
    Venue * updown = new Venue;
    updown->setName("UpDown Arcade Bar");
    updown->setDescription("Kansas City's best arcade bar!");
    prompt = "Do you want to play... ";
    str1 = "Mrs. PacMan";
    str2 = "Paper Boy";
    str3 = "Skee Ball";
    updown->setActivities(prompt, str1, str2, str3);
    updown->setCustomTrivial("The Mrs. PacMan Machine is down.");
    places.push_back(updown);
    
    /* Kauffman Stadium, season tickets are a distraction. */
    Venue * kauffman = new Venue;
    kauffman->setName("Kauffman Stadium");
    kauffman->setDescription("Home of the Kansas City Royals!");
    prompt = "Do you want to buy... ";
    str1 = "season tickets for 2016";
    str2 = "a ticket to Tour the Hall of Fame Museum";
    str3 = "a slugger bobblehead";
    kauffman->setActivities(prompt, str1, str2, str3);
    kauffman->setCustomTrivial("Box office is closed to watch the game.");
    places.push_back(kauffman);
    
    /* Kansas City Zoo, zoo ticket is a distraction. */
    Venue * zoo = new Venue;
    zoo->setName("Kansas City Zoo");
    zoo->setDescription("Come see the animals or visit Starlight Theater!");
    prompt = "Welcome to the zoo... ";
    str1 = "buy a single ticket to the zoo.";
    str2 = "shop the gift store";
    str3 = "buy a season pass to the zoo for 2016";
    zoo->setActivities(prompt, str1, str2, str3);
    zoo->setCustomTrivial("The zoo is closed.");
    places.push_back(zoo);
    
    /* Crossroads Art District, No item available. */
    Venue * crossroads = new Venue;
    crossroads->setName("Crossroads Art District");
    crossroads->setDescription("Come Enjoy First Fridays!");
    prompt = "So sorry, you can only walk around."; /* Never should be called */
    str1 = "";                                      /* Never should be called */
    str2 = "";                                      /* Never should be called */
    str3 = "";                                      /* Never should be called */
    crossroads->setActivities(prompt, str1, str2, str3);
    crossroads->setCustomTrivial("Art shows are only on the first Friday of the month.");
    places.push_back(crossroads);

    str1 = "";
    str2 = "";
    str3 = "";
    prompt = "";
    
    /*
     * Shops
     */

    /* Zona Rosa, distraction of buying goods */
    Shop * zona = new Shop;
    zona->setName("Zona Rosa Outdoor Mall");
    zona->setDescription("Kansas City's best Outdoor Mall!");
    str1 = "Game Stop";
    str2 = "Dick's Sporting Goods";
    zona->setStores(str1, str2);
    zona->setCustomTrivial("Zona Rosa is closed.");
    places.push_back(zona);
    
    /* Country Club Plaza, distraction of buying fine jewelry */
    Shop * plaza = new Shop;
    plaza->setName("Country Club Plaza");
    plaza->setDescription("Kansas City's best for fine dining and shopping!");
    str1 = "Would you like to buy some jewelry from Hezlberg Diamonds?";
    str2 = "Why not shop at North Face outlet?";
    plaza->setStores(str1, str2);
    plaza->setCustomTrivial("The Plaza has a curfew and is closed.");
    places.push_back(plaza);
    
    /* The Bunker, get Free tokens to UpDown */
    Shop * bunker = new Shop;
    bunker->setName("The Bunker");
    bunker->setDescription("Come shop local apparel!");
    str1 = "Browse local t-shirts.";
    str2 = "Ask the cashier for local hotspots";
    bunker->setStores(str1, str2);
    bunker->setCustomTrivial("Closed! We're sold out of everything!");
    places.push_back(bunker);
    
    str1 = "";
    str2 = "";
    str3 = "";
    prompt = "";
    
    /*
     * Restaurant
     */
    
    /* Lulu's, get discount at The Bunker */
    Restaurant * lulus = new Restaurant;
    lulus->setName("Lulu's Thai Noodle Shop");
    lulus->setDescription("Kansas City's favorite Thai food!");
    str1 = "Pineapple Chicken Fried Rice";
    str2 = "Crab Rangoon";
    lulus->setFoods(str1, str2);
    lulus->setCustomTrivial("You'll have to come again for Curried Coconut Shrimp!");
    places.push_back(lulus);
    
    /* Waldo Pizza, get distraction of free tickets to the Zoo */
    Restaurant * pizza = new Restaurant;
    pizza->setName("Waldo Pizza");
    pizza->setDescription("Kansas City's best pizza joint!");
    str1 = "Traditional Hand Tossed Pizza";
    str2 = "Boulevard Beer (Locally Brewed)";
    pizza->setFoods(str1, str2);
    pizza->setCustomTrivial("Next time get the Toasted Raviolli!");
    places.push_back(pizza);
    
    
    /* Gojo, get distraction of free drinks at the river market */
    Restaurant * gojo = new Restaurant;
    gojo->setName("Gojo Japanese Steakhouse");
    gojo->setDescription("Stop by for some amazing hibachi!");
    str1 = "Chicken and Steak hibachi";
    str2 = "The Special (cocktail)";
    gojo->setFoods(str1, str2);
    gojo->setCustomTrivial("Be sure to add the yellow sauce to your rice!");
    places.push_back(gojo);
    
    
    
    
    /*
     * SET PLACES
     *      Set Neighbor Places: Each place has 4 two-way connections
     */
    
    /* UpDown:          1 - Kauffman, 2 - Crossroads, 3 - Lulus, 4 - Zona
     * Kauffman:        1 - Zona, 2 - Zoo, 3 - Crossroads, 4 - UpDown
     * Zoo:             1 - Crossroads, 2 - Kauffman, 3 - Pizza, 4 - Gojo
     * Crossroads:      1 - Zona, 2 - Kauffman, 3 - Zoo, 4 - UpDown
     * Zona:            1 - Kauffman, 2 - Crossroads, 3 - UpDown, 4 - Lulus
     * Plaza:           1 - Bunker, 2 - Gojo, 3 - Pizza, 4 - Lulus
     * Bunker:          1 - Lulus, 2 - Gojo, 3 - Pizza, 4 - Plaza
     * Lulus:           1 - Zona, 2 - UpDown, 3 - Bunker, 4 - Plaza
     * Pizza:           1 - Gojo, 2 - Zoo, 3 - Plaza, 4 - Bunker
     * Gojo:            1 - Bunker, 2 - Zoo, 3 - Pizza, 4 - Plaza */
    
    updown->setPlaces(kauffman, crossroads, lulus, zona);
    kauffman->setPlaces(zona, zoo, crossroads, updown);
    zoo->setPlaces(crossroads, kauffman, pizza, gojo);
    crossroads->setPlaces(zona, kauffman, zoo, updown);
    zona->setPlaces(kauffman, crossroads, updown, lulus);
    plaza->setPlaces(bunker, gojo, pizza, lulus);
    bunker->setPlaces(lulus, gojo, pizza, plaza);
    lulus->setPlaces(zona, updown, bunker, plaza);
    pizza->setPlaces(gojo, zoo, plaza, bunker);
    gojo->setPlaces(bunker, zoo, pizza, plaza);

    
    /* 
     * ADD ITEMS TO LOCATIONS:
     *      Add Hidden Items to All Locations, including redeemableItems as necessary
     * 
     */
    
    std::string name = "";
    std::string description = "";
    Item * tempItem;
    
    /* 
     * Add KEY ITEMS
     */
    
    /* UpDown KEY Item for use at Waldo Pizza */
    name = "Coupon to Waldo Pizza";
    description = "Enjoy complimentary 14\" pizza from Waldo Pizza\nduring ";
    description += "a post-season Royals Game!";
    tempItem = new Item(updown);
    updown->setItem(tempItem);
    updown->returnItem();
    tempItem->setName(name);
    tempItem->setDescription(description);
    pizza->setRedeemableItem(tempItem);
    keyItems.push_back(tempItem);
    
    /* Bunker KEY Item for use at UpDown*/
    name = "Free tokens to UpDown";
    description = "Welcome to Kansas City! Have you ate at Lulu's yet?\n";
    description += "I love that place. Also, you should really try\n";
    description += "to go to UpDown. It's a new bar and arcade with\n";
    description += "great beverages and pizza. ";
    description += "\n\nHere, have some extra tokens!";
    tempItem = new Item(bunker);
    bunker->setItem(tempItem);
    bunker->returnItem();
    tempItem->setName(name);
    tempItem->setDescription(description);
    updown->setRedeemableItem(tempItem);
    keyItems.push_back(tempItem);
    
    /* Lulu's KEY Item for use at Bunker */
    name = "Bunker 10% discount";
    description = "Explore West Port and visit The Bunker for your\nfavorite local ";
    description += "apparel.";
    tempItem = new Item(lulus);
    lulus->setItem(tempItem);
    lulus->returnItem();
    tempItem->setName(name);
    tempItem->setDescription(description);
    bunker->setRedeemableItem(tempItem);
    keyItems.push_back(tempItem);

    
    /* Restaurant: Waldo Pizza, Root For Royals. */
    name = "Cheer for Royals";
    description = "Cheer for the Royals in the 2015 World Series!";
    tempItem = new Item(pizza);
    pizza->setItem(tempItem);
    pizza->returnItem();
    tempItem->setName(name);
    tempItem->setDescription(description);
    kauffman->setRedeemableItem(tempItem);
    keyItems.push_back(tempItem);
    
    /*
     * Add other items as distractions
     */
    
    /* Venue: Kauffman Item, Can't Redeem*/
    name = "Royals Season Tickets";
    description = "Season tickets for the 2016 seasons!";
    tempItem = new Item(kauffman);
    kauffman->setItem(tempItem);
    kauffman->returnItem();
    tempItem->setName(name);
    tempItem->setDescription(description);
    
    /* Venue: Zoo Item, Can't Redeem*/
    name = "KC Zoo Single Ticket";
    description = "One ticket to visit the KC Zoo!";
    tempItem = new Item(zoo);
    zoo->setItem(tempItem);
    zoo->returnItem();
    tempItem->setName(name);
    tempItem->setDescription(description);
    
    /* Venue: Crossroads Art District, No item available. */
    
    /* Shop: Zona Rosa Item, Can't Redeem */
    name = "New Royal's Gear";
    description = "A nice new royals hat and t-shirt";
    tempItem = new Item(zona);
    zona->setItem(tempItem);
    zona->returnItem();
    tempItem->setName(name);
    tempItem->setDescription(description);

    /* Shop: Plaza Item, Can't Redeem*/
    name = "Complimentary Movie Tickets";
    description = "See a newly released movie for free next week!";
    tempItem = new Item(plaza);
    plaza->setItem(tempItem);
    plaza->returnItem();
    tempItem->setName(name);
    tempItem->setDescription(description);
    
    /* Restaurant: Gojo Item, Can't Redeem*/
    name = "Free drink";
    description = "Go to Bo Ling's at the City River Market next month!";
    tempItem = new Item(gojo);
    gojo->setItem(tempItem);
    gojo->returnItem();
    tempItem->setName(name);
    tempItem->setDescription(description);

    
}
