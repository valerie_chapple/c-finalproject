#########################################################
# File: Makefile
# Description: A more robust makefile for this class.
# You need to edit the variables under "USER SECTION".
# To execute the makefile on unix-y systems, type 'make'.
# If that doesn't work, 'make -f makefile' might be useful.
#
# Edit: Adds .PHONY target so in case there is a file called
#       clean, the clean command will be called instead
# Edit: Adds object files for each source
#########################################################

# #'s are line-comments 

# CXX is a standard variable name used for the compiler. 
CXX = g++

# CXXFLAGS is a standard variable name for compile flags.
# -std=c++0x specifies to use a certain language version.
CXXFLAGS = -std=c++0x

# -Wall turns on all warnings
CXXFLAGS += -Wall -Wextra -Werror

# -pedantic-errors strictly enforces the standard
CXXFLAGS += -pedantic-errors

# -g turns on debug information, -O3 is optimization
CXXFLAGS += -g
# CXXFLAGS += -O3

####################
### USER SECTION ###
####################

# SRCS is a standard name for the source files. 
# Edit as needed.
SRC0 = validation.cpp
SRC1 = main.cpp
SRC2 = Game.cpp
SRC3 = Item.cpp
SRC4 = Location.cpp
SRC5 = Restaurant.cpp
SRC6 = Shop.cpp
SRC7 = Venue.cpp
SRCS = ${SRC0} ${SRC1} ${SRC2} ${SRC3} ${SRC4} ${SRC5} ${SRC6} ${SRC7}

# OBJS for the object files. 
OBJS = $(SRCS:.cpp=.o)

# HEADERS is a standard name for the header files. 
# Edit these as needed.
HEADER0 = validation.hpp
HEADER1 = Game.hpp
HEADER2 = Item.hpp
HEADER3 = Location.hpp
HEADER4 = Restaurant.hpp
HEADER5 = Shop.hpp
HEADER6 = Venue.hpp
HEADERS = ${HEADER0} ${HEADER1} ${HEADER2} ${HEADER3} ${HEADER4} ${HEADER5} ${HEADER6}

# These will be your executable names. 
# Edit as needed.
PROG1 = final
PROG2 = 
PROG3 = 
PROGS = ${PROG1} ${PROG2} ${PROG3}

# This is the name of your document(s). 
# Edit as needed.
DOC1 = final_project.pdf
DOC2 = cheatsheet.txt
DOC3 = mapfile.txt
DOC4 = kclogo.txt
DOCDATES = date0.txt date1.txt date2.txt
DOCS = ${DOC1} ${DOC2} ${DOC3} ${DOC4} ${DOCDATES}

# This is the name of your compressed file. 
# Edit name as needed. Keep the format.
TAR = final_project.tar.bz2

#####################
### BUILD SECTION ###
#####################

# Typing 'make' in terminal calls the first build available.
# In this case, default (PROG1).
default: ${OBJS} ${HEADERS}
	${CXX} ${LDFLAGS} ${OBJS} -o ${PROG1}

# Typing ‘all’ in terminal builds all files not up to date.
all: ${SRCS} ${PROGS}

# Typing executable name in terminal builds all not up to date.
${PROGS}: ${OBJS} ${HEADERS}
	${CXX} ${LDFLAGS} ${OBJS} -o $@

# Typing object name in terminal builds target based on dependencies.
validation.o: validation.cpp validation.hpp
	${CXX} ${CXXFLAGS} -c $(@:.o=.cpp)
main.o: main.cpp Game.hpp validation.hpp
	${CXX} ${CXXFLAGS} -c $(@:.o=.cpp)
Game.o: Game.cpp Game.hpp Location.hpp Venue.hpp Shop.hpp Restaurant.hpp validation.hpp
	${CXX} ${CXXFLAGS} -c $(@:.o=.cpp)
Item.o: Item.cpp Item.hpp validation.hpp
	${CXX} ${CXXFLAGS} -c $(@:.o=.cpp)
Location.o: Location.cpp Location.hpp validation.hpp
	${CXX} ${CXXFLAGS} -c $(@:.o=.cpp)
Restaurant.o: Restaurant.cpp Restaurant.hpp validation.hpp
	${CXX} ${CXXFLAGS} -c $(@:.o=.cpp)
Shop.o: Shop.cpp Shop.hpp validation.hpp
	${CXX} ${CXXFLAGS} -c $(@:.o=.cpp)
Venue.o: Venue.cpp Venue.hpp validation.hpp
	${CXX} ${CXXFLAGS} -c $(@:.o=.cpp)

# Typing 'make tar' in terminal calls this build.
# This creates a compressed file for submission.
tar:
	tar cvjf ${TAR} ${SRCS} ${HEADERS} ${DOCS} makefile

# Typing 'make clean' calls this build.
# It's designed to clean up the folder.
# Be careful with this, edit as needed.
.PHONY : clean
clean: 
	rm -f ${PROGS} *.o *~

