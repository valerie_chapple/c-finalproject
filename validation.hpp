/****************************************************************************************
 ** Program Filename: validation.hpp
 ** Author: Valerie Chapple
 ** Date: January 17, 2016 (updated Feb 16, 2016)
 **
 ** Description:    main functions header file
 ** Input:          User data
 ** Output:         Validation feedback on data entry.
 ***************************************************************************************/

#include <string>

#ifndef VALIDATION_HPP
#define VALIDATION_HPP

void clear();
void press_enter();
int validate_positive_integer(std::string prompt, int max_integer = -1);
int validate_non_neg_integer(std::string prompt, int max_integer = -1);
int validate_integer(bool &runLoop);
double validate_non_neg_double(std::string prompt);
double validate_double(bool &runLoop);
std::string getNonBlankAlphaNumUserString(std::string prompt);
std::string getNonBlankAlphaNumUserStringNoSpace(std::string prompt);
std::string intToString(int input);

#endif
