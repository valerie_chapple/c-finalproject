/****************************************************************************************
 ** Program Filename: Venue.hpp
 ** Author: Valerie Chapple
 ** Date: March 9, 2016
 **
 ** Description:    Venue Class for octoBeR AlwaYs: Inherits from Abstract Location Class
 **
 ** Input:
 **
 ** Output:
 **
 ***************************************************************************************/

#ifndef VENUE_HPP
#define VENUE_HPP
#include "Location.hpp"
#include "Item.hpp"

#include <string>

class Venue : public Location {
private:
    std::string prompt;
    std::string activity1;
    std::string activity2;
    std::string activity3;
public:
    Venue();
    ~Venue();
    
    void converse();                    /* Inherited virtual function from Location */
    void special();
    
    void setActivities(std::string prompt, std::string, std::string, std::string);
    std::string getPrompt();
    std::string getActivity1();
    std::string getActivity2();
    std::string getActivity3();
};

#endif