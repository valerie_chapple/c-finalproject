/****************************************************************************************
 ** Program Filename: Item.cpp
 ** Author: Valerie Chapple
 ** Date: March 11, 2016
 **
 ** Description:    Implementaiton of the Item Class
 ** Input:          Item name, description, and start location
 ** Output:         Generic holder for items or information possibly collected by player
 ***************************************************************************************/

#include "Item.hpp"
#include "validation.hpp"
#include <iostream>
#include <string>

/****************************************************************************************
 ** Function:         Item()
 ** Description:      Default Constructor
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Item instantiated with initialized values, no start location
 ***************************************************************************************/
Item::Item() {
    this->name = "Item";
    this->description = "Item Description";
    this->startLocation = NULL;
}



/****************************************************************************************
 ** Function:         Item()
 ** Description:      Constructor
 ** Parameters:       Location pointer to starting location
 ** Pre-Conditions:   None
 ** Post-Conditions:  Item instantiated with initialized values and start Location set
 ***************************************************************************************/
Item::Item(Location * startLocation){
    this->name = "Item";
    this->description = "Item Description";
    this->startLocation = startLocation;
}



/****************************************************************************************
 ** Function:         ~Item()
 ** Description:      Deconstructor
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Item destroyed
 ***************************************************************************************/

Item::~Item() {
    /* Note: Location memory deleted in Game Class data member vector, called places */
}



/****************************************************************************************
 ** Function:         getName()
 ** Description:      Returns name of item
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns name of item
 ***************************************************************************************/
std::string Item::getName(){
    return this->name;
}



/****************************************************************************************
 ** Function:         setName()
 ** Description:      Sets name of item
 ** Parameters:       string for name
 ** Pre-Conditions:   None
 ** Post-Conditions:  Sets name of item
 ***************************************************************************************/
void Item::setName(std::string name){
    this->name = name;
}



/****************************************************************************************
 ** Function:         getDescription()
 ** Description:      Returns description of item
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns description of item
 ***************************************************************************************/
std::string Item::getDescription(){
    return this->description;
}



/****************************************************************************************
 ** Function:         setDescription()
 ** Description:      Sets description of item
 ** Parameters:       string for description
 ** Pre-Conditions:   None
 ** Post-Conditions:  Sets description of item
 ***************************************************************************************/
void Item::setDescription(std::string description){
    this->description = description;
}



/****************************************************************************************
 ** Function:         getStartLocation()
 ** Description:      Returns starting location of item
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns starting location of item
 ***************************************************************************************/
Location * Item::getStartLocation(){
    return this->startLocation;
}


