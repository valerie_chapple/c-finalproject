/****************************************************************************************
 ** Program Filename: Location.cpp
 ** Author: Valerie Chapple
 ** Date: March 11, 2016
 **
 ** Description:    Implementation of the Location Class
 ** Input:          User chooses next location based on pointers of Location
 ** Output:         Shows current location, as well as choices for future locations
 ***************************************************************************************/

#include "Location.hpp"
#include "validation.hpp"

#include <iostream>
#include <string>


/****************************************************************************************
 ** Function:         Location()
 ** Description:      Default Constructor
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Location instantiated with initialized values.
 ***************************************************************************************/

Location::Location() {
    this->name = "Location Name";
    this->description = "Location Description";
    this->p1 = NULL;
    this->p2 = NULL;
    this->p3 = NULL;
    this->p4 = NULL;
    
    this->type = 0;
    this->specialTaskDone = false;
    this->hiddenItem = NULL;
    this->itemGone = true;
    this->redeemableItem = NULL;
    
    this->visitStatus = 0;
    this->customTrivial = "Trivial Convo.";
}



/****************************************************************************************
 ** Function:         ~Location()
 ** Description:      Deconstructor
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Location destroyed
 ***************************************************************************************/

Location::~Location() {
    if (hiddenItem != NULL) {
        delete hiddenItem;
        hiddenItem = NULL;
    }
}


/****************************************************************************************
 ** Function:         interact()
 ** Description:      Presents character dialogue
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  No Return
 ***************************************************************************************/
void Location::interact(){
    clear();
    if (hiddenItem != NULL){
        /* Item has been assigned, but may or may not be hidden */
        converse();
    }
    else {
        /* Used only if Location does not have an item assigned */
        std::cout << "\nYou're at " << this->name << "!\n\b";
        std::cout << this->customTrivial << std::endl << std::endl;
        press_enter();
    }
    
}




/****************************************************************************************
 ** Function:         chooseNextLocation()
 ** Description:      Prompts user to choose next location
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Pointer Location returned.
 ***************************************************************************************/
Location * Location::chooseNextLocation(){
    Location * temp = NULL;
    int choice = 0;
    std::cout << std::endl;
    std::cout << "You are at: " << this->name << std::endl;
    std::cout << std::endl;
    std::string prompt = "Options:\n";
    if (p1 != NULL) {
        prompt += "\t1 - ";
        prompt += p1->getName();
        prompt += "\n";
    }
    if (p2 != NULL) {
        prompt += "\t2 - ";
        prompt += p2->getName();
        prompt += "\n";
    }
    if (p3 != NULL) {
        prompt += "\t3 - ";
        prompt += p3->getName();
        prompt += "\n";
    }
    if (p4 != NULL) {
        prompt += "\t4 - ";
        prompt += p4->getName();
        prompt += "\n";
    }
    prompt += "\t0 - Stay here\n";
    
    std::cout << std::endl;
    prompt += "Where would you like to go next? ";
    choice = validate_non_neg_integer(prompt, 4);
    switch (choice) {
        case 1:
            if (p1 != NULL) {temp = p1;}
            else {
                std::cout << "Invalid Entry." << std::endl;
                temp = this;
            }
            break;
        case 2:
            if (p2 != NULL) {temp = p2;}
            else {
                std::cout << "Invalid Entry." << std::endl;
                temp = this;
            }
            break;
        case 3:
            if (p3 != NULL) {temp = p3;}
            else {
                std::cout << "Invalid Entry." << std::endl;
                temp = this;
            }
            break;
        case 4:
            if (p4 != NULL) {temp = p4;}
            else {
                std::cout << "Invalid Entry." << std::endl;
                temp = this;
            }
            break;
        case 0:
            temp = this;
            
        default:
            break;
            std::cout << "Invalid Entry." << std::endl;
            temp = this;
    }

    return temp;
}



/****************************************************************************************
 ** Function:         setName()
 ** Description:      sets name of the location
 ** Parameters:       string for name
 ** Pre-Conditions:   None
 ** Post-Conditions:  Location name changed
 ***************************************************************************************/
void Location::setName(std::string name){
    this->name = name;
}



/****************************************************************************************
 ** Function:         getName()
 ** Description:      gets name of the location
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns the location name
 ***************************************************************************************/
std::string Location::getName(){
    return this->name;
}



/****************************************************************************************
 ** Function:         setDescription()
 ** Description:      sets description of the location
 ** Parameters:       string for description
 ** Pre-Conditions:   None
 ** Post-Conditions:  Location description changed
 ***************************************************************************************/
void Location::setDescription(std::string description){
    this->description = description;
}



/****************************************************************************************
 ** Function:         getDescription()
 ** Description:      gets description of the location
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns the location description
 ***************************************************************************************/
std::string Location::getDescription(){
    return this->description;
}



/****************************************************************************************
 ** Function:         setPlaces()
 ** Description:      sets places of the location
 ** Parameters:       Location pointers (4)
 ** Pre-Conditions:   None
 ** Post-Conditions:  Location pointer changed
 ***************************************************************************************/

void Location::setPlaces(Location * p1, Location * p2, Location * p3, Location * p4){
    this->p1 = p1;
    this->p2 = p2;
    this->p3 = p3;
    this->p4 = p4;
}


/****************************************************************************************
 ** Function:         getP1()
 ** Description:      gets location pointer
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns the location pointer
 ***************************************************************************************/
Location * Location::getP1(){
    return this->p1;
}




/****************************************************************************************
 ** Function:         getP2()
 ** Description:      gets location pointer
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns the location pointer
 ***************************************************************************************/
Location * Location::getP2(){
    return this->p2;
}




/****************************************************************************************
 ** Function:         getP3()
 ** Description:      gets location pointer
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns the location pointer
 ***************************************************************************************/
Location * Location::getP3(){
    return this->p3;
}



/****************************************************************************************
 ** Function:         getP4()
 ** Description:      gets location pointer
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns the location pointer
 ***************************************************************************************/
Location * Location::getP4(){
    return this->p4;
}



/****************************************************************************************
 ** Function:         getType()
 ** Description:      gets integer type of Location 0 - Restaurant, 1 - Store, 2 - Venue
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns the location type
 ***************************************************************************************/
int Location::getType(){
    return this->type;
}




/****************************************************************************************
 ** Function:         isSpecialTaskDone()
 ** Description:      gets bool of special task being completed at least once.
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns boolean if special task is true or false
 ***************************************************************************************/
bool Location::isSpecialTaskDone(){
    return specialTaskDone;
}




/****************************************************************************************
 ** Function:         setItem(Item * itemIn)
 ** Description:      sets Item pointer of the location
 ** Parameters:       Item pointer
 ** Pre-Conditions:   None
 ** Post-Conditions:  Item pointer changed
 ***************************************************************************************/
void Location::setItem(Item * itemIn){
    hiddenItem = itemIn;
}



/****************************************************************************************
 ** Function:         getItem()
 ** Description:      gets Item pointer of the location
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns item pointer
 ***************************************************************************************/
Item * Location::getItem(){
    return this->hiddenItem;
}



/****************************************************************************************
 ** Function:         isItemGone()
 ** Description:      Returns status of the item
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns true if item is gone, false if item is still there
 ***************************************************************************************/
bool Location::isItemGone(){
    return itemGone;
}



/****************************************************************************************
 ** Function:         returnItem()
 ** Description:      Changes the status of the item
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Changes itemGone to false
 ***************************************************************************************/
void Location::returnItem(){
    this->itemGone = false;
}



/****************************************************************************************
 ** Function:         removeItem()
 ** Description:      Changes the status of the item
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Changes itemGone to true
 ***************************************************************************************/
void Location::removeItem(){
    this->itemGone = true;
}



/****************************************************************************************
 ** Function:         setRedeemableItem()
 ** Description:      Sets the redeemableItem than can unlock options.
 ** Parameters:       Item pointer to be key item for the location
 ** Pre-Conditions:   None
 ** Post-Conditions:  Sets a pointer to Item for redeemableItem
 ***************************************************************************************/
void Location::setRedeemableItem(Item * item){
    this->redeemableItem = item;
}


/****************************************************************************************
 ** Function:         getRedeemableItem()
 ** Description:      Returns the pointer to redeemableItem than can unlock options.
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns Item pointer for redeemableItem for the location
 ***************************************************************************************/
Item * Location::getRedeemableItem(){
    return this->redeemableItem;
}




/****************************************************************************************
 ** Function:         getVisitStatus()
 ** Description:      gets visit status
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns integer: 0 never been, 1 no convo, 2 - repeat convo
 ***************************************************************************************/
int Location::getVisitStatus(){
    return this->visitStatus;
}



/****************************************************************************************
 ** Function:         setVisitStatus(int)
 ** Description:      sets visit status
 ** Parameters:       set integer: 0 never been, 1 no convo, 2 - repeat convo
 ** Pre-Conditions:   None
 ** Post-Conditions:  Sets status integer
 ***************************************************************************************/
void Location::setVisitStatus(int status){
    this->visitStatus = status;
}




/****************************************************************************************
 ** Function:         getCustomText2()
 ** Description:      gets Custom text: Trivial statement b/c no item to give
 ** Parameters:       None
 ** Pre-Conditions:   No item to give; check visit status and itemGone before calling
 ** Post-Conditions:  Returns string for no item to give
 ***************************************************************************************/
std::string Location::getCustomTrivial(){
    return this->customTrivial;
}


/****************************************************************************************
 ** Function:         setCustomTrivial()
 ** Description:      sets Custom text: Repeat Visitor no item
 ** Parameters:       string for custom text
 ** Pre-Conditions:   None
 ** Post-Conditions:  Sets text for a repeat visitor but no item to give
 ***************************************************************************************/
void Location::setCustomTrivial(std::string text){
    this->customTrivial = text;
}