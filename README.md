# README #
Copyright: Valerie Chapple (March 25, 2016)

### Final Project: octoBeR AlwaYs ###
This is the final project for my C++ second term course. Utilizes pointers, inheritance, and polymorphism.  The text-based game requires the player to navigate through various locations, such as restaurants, venues, and shops, in Kansas City.  The player must visit particular locations to collect items and redeem them elsewhere. Also, the player must interact at least once at a restaurant, venue, or shop. Once all tasks are completed and key items redeemed, the game is over.

Version 1.0

### Makefile ###

* A makefile is included to be used with g++
* command line: 'make final'
* g++ compiler, c++98 compatible

### Libraries ###
* iostream
* fstream
* string
* vector

### Feedback ###

This is my first self-created project during my second term of coursework. 

I am open to constructive criticism, suggestions, and tips for this project.

### Contact ###

Valerie Chapple - chapplev@gmail.com - https://twitter.com/capnchapple