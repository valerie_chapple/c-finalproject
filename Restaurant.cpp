/****************************************************************************************
 ** Program Filename: Restaurant.cpp
 ** Author: Valerie Chapple
 ** Date: March 11, 2016
 **
 ** Description:    Implementation file for Restaurant class
 ** Input:          User can order food or drink, as well as tip a server
 ** Output:         User can collect an item, and complete the task of tipping a server
 **
 ***************************************************************************************/

#include "Restaurant.hpp"
#include "validation.hpp"

#include <iostream>
#include <string>


/****************************************************************************************
 ** Function:         Restaurant()
 ** Description:      Default Constructor
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Restaurant instantiated with initialized values.
 ***************************************************************************************/
Restaurant::Restaurant(){
    this->food1 = "";
    this->food2 = "";
    this->type = 1;
}



/****************************************************************************************
 ** Function:         ~Restaurant()
 ** Description:      Deconstructor
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Restaurant destroyed
 ***************************************************************************************/

Restaurant::~Restaurant() {
}


/****************************************************************************************
 ** Function:         converse()
 ** Description:      Converse with Restaurant
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Ultimately determines if Item is found and/or taken by player.
 ***************************************************************************************/
void Restaurant::converse(){
    int choice = -1 ;
    
    std::string prompt = "\nYum! Everything looks so good!\n";
    prompt += "I recommend one of the following:\n";
    prompt += "\t1 - ";
    prompt += food1;
    prompt += "\n";
    prompt += "\t2 - ";
    prompt += food2;
    prompt += "\n";
    prompt += "\t0 - Back\n";
    prompt += "What would you like to order? ";
    
    choice = validate_non_neg_integer(prompt, 2);
    std::cout << std::endl;
    
    switch (choice) {
        case 1:
            /* Do Option 1 */
            std::cout << "My favorite!\n\n";
            press_enter();
            clear();
            special();
            std::cout << std::endl;
            break;
        case 2:
            /* Do Option 2 and get Hidden item */
            std::cout << "Excellent choice!\n";
            if ((hiddenItem != NULL) && (!itemGone)) {
                std::cout << "Look there's a deal!\n";
                
                /* Prompt to accept item */
                std::cout << "\n" << hiddenItem->getName() << ":\n";
                std::cout << "\"" << hiddenItem->getDescription() << "\"\n\n";
                prompt = "Do you want to add the item? [0 - no, 1 - yes] ";
                int addItemAnwswer = -1;
                addItemAnwswer = validate_non_neg_integer(prompt, 1);
                if (addItemAnwswer == 1) {
                    itemGone = true;
                }
            }
            else {
                std::cout << "Still Hungry? Wow, go for it!\n";
                press_enter();
            }
            clear();
            special();
            std::cout << std::endl;
            break;
        default:
            break;
            
    }
    
    visitStatus = 1;
}



/****************************************************************************************
 ** Function:         special()
 ** Description:      special tip server at restaurant
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  changes tip to server to true if selected yet. Unchanged otherwise
 ***************************************************************************************/
void Restaurant::special(){
    /* Prompt user to */
    std::string prompt = "How much do you want to tip the server?\n";
    prompt += "\t 0 -  0%\n";
    prompt += "\t 1 -  5%\n";
    prompt += "\t 2 - 10%\n";
    prompt += "\t 3 - 15%\n";
    prompt += "\t 4 - 20%+\n";
    int choice = validate_non_neg_integer(prompt, 4);
    
    if (choice != 0){
        specialTaskDone = true;
        std::cout << "Congrats, you tipped a server!\n";
        press_enter();
    }
    
}



/****************************************************************************************
 ** Function:         setFoods
 ** Description:      sets food1, food2
 ** Parameters:       two strings for food or menu options
 ** Pre-Conditions:   None
 ** Post-Conditions:  sets string value for foods
 ***************************************************************************************/
void Restaurant::setFoods(std::string f1, std::string f2){
    this->food1 = f1;
    this->food2 = f2;
}



/****************************************************************************************
 ** Function:         getFood1(), getFood2()
 ** Description:      gets store1, store2
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns string value for food1, food2
 ***************************************************************************************/
std::string Restaurant::getFood1(){
    return this->food1;
}
std::string Restaurant::getFood2(){
    return this->food2;
}