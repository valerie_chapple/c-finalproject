/****************************************************************************************
 ** Program Filename: Shop.hpp
 ** Author: Valerie Chapple
 ** Date: March 9, 2016
 **
 ** Description:    Shop Class for octoBeR AlwaYs: Inherits from Abstract Location Class
 ** Input:          User can shop at stores/sections, as well as talk with cashier
 ** Output:         User can collect an item; complete the task of talking with cashier
 ***************************************************************************************/

#ifndef SHOP_HPP
#define SHOP_HPP
#include "Location.hpp"
#include "Item.hpp"

#include <string>

class Shop : public Location {
private:
    std::string store1;
    std::string store2;

    
public:
    Shop();
    ~Shop();
    
    void converse();            /* Inherited virtual functions from Location */
    void special();
    
    void setStores(std::string, std::string);
    std::string getStore1();
    std::string getStore2();

};

#endif