/****************************************************************************************
 ** Program Filename: Shop.cpp
 ** Author: Valerie Chapple
 ** Date: March 11, 2016
 **
 ** Description:    Operates the Shop class
 ** Input:          User can shop at stores/sections, as well as talk with cashier
 ** Output:         User can collect an item; complete the task of talking with cashier
 ***************************************************************************************/

#include "Shop.hpp"
#include "validation.hpp"

#include <iostream>
#include <string>


/****************************************************************************************
 ** Function:         Shop()
 ** Description:      Default Constructor
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Shop instantiated with initialized values.
 ***************************************************************************************/
Shop::Shop(){
    this->store1 = "";
    this->store2 = "";
    this->type = 2;
}



/****************************************************************************************
 ** Function:         ~Shop()
 ** Description:      Deconstructor
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Shop destroyed
 ***************************************************************************************/
Shop::~Shop() {
}


/****************************************************************************************
 ** Function:         converse()
 ** Description:      Converse with Shop
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Choose store, possibly collecting an item
 ***************************************************************************************/
void Shop::converse(){
    int choice = -1 ;
    
    std::string prompt = "\nWow! So many things to buy here! Where do you want to go?\n";
    
    prompt += "\t1 - ";
    prompt += store1;
    prompt += "\n";
    prompt += "\t2 - ";
    prompt += store2;
    prompt += "\n";
    prompt += "\t0 - Back\n";
    prompt += "What would you like to do? ";
    
    choice = validate_non_neg_integer(prompt, 2);
    std::cout << std::endl;
    
    switch (choice) {
        case 1:
            /* Do Option 1 */
            std::cout << "Can't ever go wrong with that!\n\n";
            press_enter();
            clear();
            special();
            std::cout << std::endl;
            break;
        case 2:
            /* Do Option 2 and get Hidden item */
            std::cout << "Great idea!\n";
            if ((hiddenItem != NULL) && (!itemGone)) {
                std::cout << "Look what you got!\n";
                
                /* Prompt to accept item */
                std::cout << "\n" << hiddenItem->getName() << ":\n";
                std::cout << "\"" << hiddenItem->getDescription() << "\"\n\n";
                prompt = "Do you want to add the item? [0 - no, 1 - yes] ";
                int addItemAnwswer = -1;
                addItemAnwswer = validate_non_neg_integer(prompt, 1);
                if (addItemAnwswer == 1) {
                    itemGone = true;
                }
            }
            else {
                std::cout << "Really? You need more?\n";
                press_enter();
            }
            clear();
            special();
            std::cout << std::endl;
            break;
        default:
            break;
            
    }
    visitStatus = 1;
}




/****************************************************************************************
 ** Function:         special()
 ** Description:      special talk to chashier at shope
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  changes specialTask to true if selected. Unchanged otherwise
 ***************************************************************************************/
void Shop::special(){
    /* Prompt user to */
    std::string prompt = "What do you want to say to the Cashier?\n";
    prompt += "\t 0 - Nothing\n";
    prompt += "\t 1 - Thanks for all you do.\n";
    prompt += "\t 2 - Have great day!\n";

    int choice = validate_non_neg_integer(prompt, 2);
    
    if (choice != 0){
        specialTaskDone = true;
        std::cout << "Congrats, you talked some shop!\n";
        press_enter();
    }
}


/****************************************************************************************
 ** Function:         setStores
 ** Description:      sets store1, store2
 ** Parameters:       two strings for store/sections
 ** Pre-Conditions:   None
 ** Post-Conditions:  sets string value for stores
 ***************************************************************************************/
void Shop::setStores(std::string s1, std::string s2){
    this->store1 = s1;
    this->store2 = s2;
}



/****************************************************************************************
 ** Function:         getStore1(), getStore2()
 ** Description:      gets store1, store2
 ** Parameters:       None
 ** Pre-Conditions:   None
 ** Post-Conditions:  Returns string value for store1, store2
 ***************************************************************************************/
std::string Shop::getStore1(){
    return this->store1;
}
std::string Shop::getStore2(){
    return this->store2;
}
