/****************************************************************************************
 ** Program Filename: main.cpp
 ** Author: Valerie Chapple
 ** Date: March 8, 2016
 **
 ** Description:    Final Project
 **
 ** Input:          None, calls Game class, where user inputs choice of location(s)
 ** Output:         Game class will display all output
 ***************************************************************************************/


#include <iostream>                 /* Required for User Input */
#include <string>                   /* Menu Support */

#include "Game.hpp"
#include "validation.hpp"

int main() {
    clear();
    
    /* Start Game */
    Game game1;
    game1.start();
    
    return 0;
}
