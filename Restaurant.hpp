/****************************************************************************************
 ** Program Filename: Restaurant.hpp
 ** Author: Valerie Chapple
 ** Date: March 9, 2016
 **
 ** Description:    Restaurant Class for octoBeR AlwaYs: Inherits from 
 **                 Abstract Location Class
 **
 ** Description:    Header file for the Restaurant class
 ** Input:          User can order food or drink, as well as tip a server
 ** Output:         User can collect an item, and complete the task of tipping a server
 ***************************************************************************************/

#ifndef RESTAURANT_HPP
#define RESTAURANT_HPP
#include "Location.hpp"
#include "Item.hpp"

#include <string>

class Restaurant : public Location {
private:
    std::string food1;
    std::string food2;
    
public:
    Restaurant();
    ~Restaurant();
    
    void converse();            /* Inherited virtual functions from Location */
    void special();
    
    void setFoods(std::string f1, std::string f2);
    std::string getFood1();
    std::string getFood2();
};

#endif