/****************************************************************************************
 ** Program Filename: Item.hpp
 ** Author: Valerie Chapple
 ** Date: March 9, 2016
 **
 ** Description:    Item Class in octoBeR AlwaYs
 **
 ** Input:          Item name, description, and start location
 ** Output:         Generic holder for items or information possibly collected by player
 ***************************************************************************************/

#ifndef ITEM_HPP
#define ITEM_HPP
#include <string>
class Location;

class Item {
private:
    std::string name;
    std::string description;
    Location * startLocation;
    
public:
    Item();
    Item(Location * startLocation);
    ~Item();
    std::string getName();
    void setName(std::string);
    std::string getDescription();
    void setDescription(std::string);
    Location * getStartLocation();
};

#endif