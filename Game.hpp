/****************************************************************************************
 ** Program Filename: Game.hpp
 ** Author: Valerie Chapple
 ** Date: March 9, 2016
 **
 ** Description:    Game Class to manage octoBeR AlwaYs
 ** Input:          User selects locations to which to travel and explore.
 ** Output:         User can win when certain items are collected and redeemed, as well
 **                 as when three particular actions are taken.
 ***************************************************************************************/

#ifndef GAME_HPP
#define GAME_HPP

#include "Location.hpp"
#include "Item.hpp"
#include <vector>
#include <unistd.h>

#define MAX_MOVES 15
#define MAX_ITEMS_CARRIED 2
#define CHEAT_SHEET_FILENAME "cheatsheet.txt"
#define MAP_FILENAME "mapfile.txt"
#define LOGO_FILENAME "kclogo.txt"
#define WIN_FILENAME0 "date0.txt"
#define WIN_FILENAME1 "date1.txt"
#define WIN_FILENAME2 "date2.txt"

class Game {
private:
    /* Data Members */
    int moveCount;
    std::vector<Location*> places;
    Location * currentLocation;                 /* Player Current Location */
    
    std::vector<Item*> items;                   /* Player Current Items Limit: 3 */
    std::vector<Item*> itemsUsed;               /* Player Items Used: 3 */
    std::vector<Item*> keyItems;                /* Set by the Game Setup */
    
    bool tipRestaurant;
    bool talkShop;
    bool playVenue;
    
    bool quitGame;
    
    /* Game Flow */
    void setup();
    void play();
    void end();
    Location * arrive();
    bool isGameOver();
    bool doKeysMatch();
    
    bool itemAlreadyCollected(Item *);
    bool itemAlreadyUsed(Item *);
    void removeItem(Item *);
    bool removeItemsMenu();
    
    void printItemsCollected();
    void printItemsUsed();
    
    void printFile(std::string);
    void playWinningScene();
    void startKansasCity();

    
    
public:
    Game();
    ~Game();                /* No Copy Constructor *//* No Assignment Overload */
    
    void start();
};

#endif